﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame
{
    public enum GraphicsAPI
    {
        OpenGL4,
        OpenGLES2,
        OpenGLES3,
        Vulkan,
        Direct3D11,
        Direct3D12,
    }
}
