﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.Assets.Importers.DDSTextureImporter
{
    internal enum DXT10MiscFlag : uint
    {
        TextureCube = 0x4,
    }
}
