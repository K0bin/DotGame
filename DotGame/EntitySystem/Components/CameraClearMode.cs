﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.EntitySystem.Components
{
    public enum CameraClearMode
    {
        Nothing = 0,
        Depth = 1,
        Color = 2,
        Skybox = 3
    }
}
