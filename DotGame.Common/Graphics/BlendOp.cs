﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DotGame.Graphics
{
    public enum BlendOp
    {
        Add, Subtract, RevSubtract, Min, Max
    }
}
