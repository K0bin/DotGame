﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.Graphics
{
    public enum StencilOperation
    {
        Keep, Zero, Replace, IncrSat, DecrSat, Invert, Incr, Decr
    }
}
