﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.Graphics
{
    public enum VertexElementType
    {
        Single, Vector2, Vector3, Vector4
    }
}
