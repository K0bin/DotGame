﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.Graphics
{
    /// <summary>
    /// Eine Texture auf der gezeichnet werden kann.
    /// </summary>
    public interface IRenderTarget2D : ITexture2D
    {
    }
}
