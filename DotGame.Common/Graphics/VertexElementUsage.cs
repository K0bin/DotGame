﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.Graphics
{
    public enum VertexElementUsage
    {
        Position, Color, Normal, TexCoord, Tangent, Binormal
    }
}
