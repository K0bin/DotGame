﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.Graphics
{
    public enum IndexFormat
    {
        Int32, UInt32, Short16, UShort16
    }
}
