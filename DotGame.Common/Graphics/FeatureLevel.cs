﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotGame.Graphics
{
    enum FeatureLevel
    {
        ES20,
        DX91,
        DX92,
        DX93,
        DX10,
        DX11,
        DX12,
    }
}
